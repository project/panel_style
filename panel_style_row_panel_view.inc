<?php

class panel_style_row_panel_view extends views_plugin_row {
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    
    $form['panel_id'] = array(
      '#type' => 'radios',
      '#title' => t('Select a mini panel to use'),
      '#default' => $this->options['panel_id'],
      '#options' => panels_mini_panels_mini_list(),
      '#description' => t('The selected mini panel will be past the node as a context which can be used to layout the content.'),
    );
  }
  
  function render($row) {
    ctools_include('context');
    
    $node = node_load($row->{$this->field_alias});
    $node->view =& $this->view;
    $mini = panels_mini_load($this->options['panel_id']);
    $context = ctools_context_create('node', $node);
    $context->identifier = t('This node');
    $context->keyword = 'node';
    
    $contexts = array('node-row' => $context);
    
    $context = ctools_context_match_required_contexts($mini->requiredcontexts, $contexts);
    $mini->context = $mini->display->context = ctools_context_load_contexts($mini, FALSE, $context);
    return panels_render_display($mini->display);
  }
}