<?php

/**
 * @file
 *   Provide views integration.
 */

/**
 * Implementation of hook_views_plugins().
 */
function panel_style_views_plugins() {
  return array(
    'row' => array(
      'panel_style' => array(
        'title' => t('Mini Panel'),
        'help' => t('Allow a mini Panel to be used display views output.'),
        'handler' => 'panel_style_row_panel_view',
        'path' => drupal_get_path('module', 'panel_style'),
        'base' => array('node'),
        'uses options' => TRUE,
        'type' => 'normal',
      ),
    ),
  );
}
